#!/usr/bin/env python
# coding: utf-8

# In[31]:


hangman_list = [
                    '''
                     ____________
                    |  /         |
                    | /          
                    |
                    |
                    |
                    |____________
                    
                    ''' ,
    
                    '''
                     ____________
                    |  /         |
                    | /         ( ) 
                    |
                    |
                    |
                    |____________
                    
                    ''' ,
    
                    '''
                     ____________
                    |  /         |
                    | /         ( )
                    |            |
                    |            |
                    |
                    |____________
                    
                    ''' ,
    
                    '''
                     ____________
                    |  /         |
                    | /         ( )
                    |            |
                    |           /|
                    |
                    |____________
                    
                    ''' ,
    
                    '''
                     ____________
                    |  /         |
                    | /         ( )
                    |            |
                    |           /|\\
                    |
                    |____________
                    
                    ''' ,
    
                    '''
                     ____________
                    |  /         |
                    | /         ( )
                    |            |
                    |           /|\\
                    |           /
                    |____________
                    
                    ''' ,
    
                    '''
                     ____________
                    |  /         |
                    | /         ( )
                    |            |
                    |           /|\\
                    |           / \\
                    |____________
                    
                    ''' 
            ]

